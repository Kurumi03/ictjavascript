@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
    <ul>
      <li class="first"><a href="/home">Home</a></li>
      <li><a href="/add">Recipes</a></li>
      <li class="selected"><a href="/">About</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/create">Create</a></li>
    </ul>
  </div>
  <div id="content">
    <div>
      <div class="aside">
        <ul>
          <li> <a href="#"><img src="images/fruit-desserts.jpg" alt=""></a> <a href="#">Pastry Fruit Desserts</a> </li>
          <li> <a href="#"><img src="images/fruit-recipes.jpg" alt=""></a> <a href="#">Fruity Recipes</a> </li>
        </ul>
      </div>
      <div>
        <h1>About the <span>healthy life</span></h1>
        <span>Must Know !</span>
        <p>All humans have to eat food for growth and maintenance of a healthy body, but we humans have different requirements as infants, children (kids), teenagers, young adults, adults, and seniors. For example, infants may require feeding every four hours until they gradually age and begin to take in more solid foods. Eventually they develop into the more normal pattern of eating three times per day as young kids. However, as most parents know, kids, teenagers, and young adults often snack between meals. Snacking is often not limited to these age groups because adults and seniors often do the same.</p>
        <span>Things to do</span>
        <p>Peel the mangoes, slice in half and then slice the cut halves lengthwise into thin strips. Set aside.
                Using an electric mixer, beat the whipping cream in a medium bowl until double in size. Add condensed milk and vanilla extract; mix until well combined.
                Put a layer of graham crackers at the bottom of an 8-inch square glass baking dish or any similar container, filling the gaps with trimmed </p>
        <span>Food to eat</span>
        <p> Here are the 12 best foods you can eat in the morning. Eggs. Share on Pinterest. Greek Yogurt. Greek yogurt is creamy, delicious and nourishing. Coffee. Coffee is an amazing beverage to start your day. Oatmeal. Oatmeal is the best breakfast choice for cereal lovers. Chia Seeds. Berries. Nuts. Green Tea.</p>
        
    
      </div>
    </div>
  </div>
  <div id="footer">
      <div>
        <div>
          <ul>
            <li> <a href="#"><img src="images/baking-fruits.jpg" alt=""></a>
              <h2>Baking Fruits</h2>
          
            </li>
            <li> <a href="#"><img src="images/health-benefits.jpg" alt=""></a>
              <h2>Health Benefits</h2>
              
            </li>
            <li> <a href="#"><img src="images/vitamins.jpg" alt=""></a>
              <h2>Vitamins in them</h2>

            </li>
          </ul>
        </div>
        <p class="footnote">The Healthy Food Life Style</p>
      </div>
    </div>
  </div>
@endsection
