@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <img src="images/logo.gif" alt=""></div>
    <ul>
      <li class="first"><a href="/home">Home</a></li>
      <li class="selected"><a href="/">Recipes</a></li>
      <li><a href="/about">About</a></li>
      <li><a href="/create">Create</a></li>
      <li><a href="/blog">Blog</a></li>
    </ul>
  </div>
  <br>
  <br>
  <div id="content">
        <div>
          <div class="aside">
            <ul>
         <li class="first"><img style="width:100%" src="/storage/cover_images/{{$add->cover_image}}"><center>Try Now !!!</center ></li>
        </ul>
    </div>
    <div>
<h2 class="first">{{$add->title}}</h2>
    {{$add->description}}
</div>
</div>

<hr><center>
        <div class="btn-group">
        <button class="button"><a href="/add" class="back">Go Back</a></button>
        <button class="button"><a href="/add/{{$add->id}}/edit" class="btn btn-default">Edit</a></button>
    
{!! Form::open(['action' => ['AddController@destroy', $add->id], 'method' => 'adds','class'=>'pull-right']) !!}
{{Form::hidden('_method','DELETE')}}
{{Form::submit('Delete', ['class'=>'button'])}}
{!! Form::close() !!}
</div>
</center>
<hr>
</div>
@endsection