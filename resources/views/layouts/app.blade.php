<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
<title>The Healthy Life</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/style.css">
</head>
<body>
    @yield('content')
    @include('messages')
</body>
</html>