@extends('layouts.app')

@section('content')
<div id="page">
  <div id="header">
    <div> <a href="#"><img src="images/logo.gif" alt=""></a> </div>
    <ul>
      <li class="first"><a href="/home">Home</a></li>
      <li><a href="/add">Recipes</a></li>
      <li><a href="/about">About</a></li>
      <li class="selected"><a href="/">Blog</a></li>
      <li><a href="/create">Create</a></li>
    </ul>
  </div>
  <div id="content">
    <div id="blog">
      <div class="aside">
        <ul>
          <li class="first">
            <h2>Archives</h2>
            <h3>2011 <span>(60)</span></h3>
            <div>
              <p>November <span>(11)</span></p>
              <p>October <span>(3)</span></p>
              <p>September <span>(8)</span></p>
              <p>August <span>(3)</span></p>
              <p>July <span>(2)</span></p>
              <p>June</p>
              <p>May <span>(8)</span></p>
              <p>April <span>(7)</span></p>
              <p>March <span>(7)</span></p>
              <p>April <span>(7)</span></p>
              <p>February <span>(10)</span></p>
              <p>January <span>(1)</span></p>
            </div>
            <h3>2010</h3>
            <h3>2009</h3>
          </li>
        </ul>
      </div>
      <div>
        <ul>
          <li> <span id="date">Dec 17</span> <span class="connect"> <a href="#" class="twitter">Twitter</a> <a href="#" class="heart">Heart</a> <a href="#" class="facebook">Facebook</a> </span>
            <h1><a href="#">Newest Recipe with a Twist</a></h1>
            <h2 class="first">French Fries</h2>
            <img src="images/fries.jpg" alt=""> <b>5 ripe mangoes or more as needed,</b> <b>2 cups whipping cream</b> <b>1/2 cup sweetened condensed milk</b> <b>1/2 tsp vanilla extract</b> <br/>
            <br><br><br><br><br>
    
          </li>
          <li> <span id="date">Nov 03</span> <span class="connect"> <a href="#" class="twitter">Twitter</a> <a href="#" class="heart">Heart</a> <a href="#" class="facebook">Facebook</a> </span>
            <h1><a href="#">Christmas is coming!</a></h1>
            <h2>Christmas Recipes on the go...</h2>
            <p>Panettone (known locally as pan dulce) and turrón are the most popular Christmas sweets in Argentina regardless of socioeconomic status, with 76% of Argentines choosing the former and 59% the latter in 2015.[1] Mantecol, a typical peanut dessert, is also popular, being favored by 49% of Argentines in the same survey.[2] Sparkling wines, ciders and frizzantes concentrate most of their sales during Christmas season; sparkling wine is mostly consumed by small families with high and medium socioeconomic status living in Greater Buenos Aires and the country's largest cities, while cider and frizzantes are popular among lower classes and large families.</p>
          </li>
        </ul>
        <div class="section"> </div>
      </div>
    </div>
  </div>
  <div id="footer">
      <div>
        <div>
          <ul>
            <li> <a href="#"><img src="images/baking-fruits.jpg" alt=""></a>
              <h2>Baking Fruits</h2>
          
            </li>
            <li> <a href="#"><img src="images/health-benefits.jpg" alt=""></a>
              <h2>Health Benefits</h2>
              
            </li>
            <li> <a href="#"><img src="images/vitamins.jpg" alt=""></a>
              <h2>Vitamins in them</h2>

            </li>
          </ul>
        </div>
        <p class="footnote">The Healthy Food Life Style</p>
      </div>
    </div>
  </div>
@endsection
